const request = require('supertest');
const app = require('../app');

describe('App', function() {
  it('renders the default page', function(done) {
    request(app)
      .get('/')
      .expect(/Welcome to Express/, done);
  });
}); 
